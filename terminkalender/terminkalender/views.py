from django.shortcuts import render, redirect


from .models import Duty
# Create your views here.


def hinzufugen(request):
    duty = Duty()
    if "hinzufügen" in request.POST:
        
        duty.title = request.POST.get("title")
        duty.note = request.POST.get("notes")
        duty.due_date = request.POST.get("due_date")
        

        duty.save()

    return redirect("anzeigen")


        
    


def löschen(request):
    pass


def bearbeiten(request):
    pass



def anzeigen(request):
    duty_all = Duty.objects.all()
    return render(request, "terminkalender/index.html", {"duty_all": duty_all})



def erledigen(request):
    pass