from django.db import models
from datetime import datetime, timezone, timedelta

# Create your models here.

class Duty(models.Model):
    title = models.CharField(max_length = 200)
    note = models.TextField()
    due_date = models.DateTimeField()
    completed = models.BooleanField(default=False) 
    
    def __str__(self):
        return "{}, {}".format(self.title, self.id)

    def time_left(self):
        time = datetime.now(timezone.utc) - self.due_date
        print(time)
    
        return "{}".format(time)

    def time_difference(self):
        result = time_left() - timedelta(2)
        return "{}".format(result)

    



