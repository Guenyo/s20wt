from django.apps import AppConfig


class TerminkalenderConfig(AppConfig):
    name = 'terminkalender'
