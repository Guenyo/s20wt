from django.shortcuts import render, redirect
from django.http import HttpResponse

from . import models

# Create your views here.

def index(request):
    items = models.Item.objects.all()
    return render(request, "shopping/index.html", {"items":items})

def speichern(request):
    if 'speichern' in request.POST:
        text = request.POST['eingabe']
        menge = request.POST["menge"]
        if text and menge:
            item = models.Item(artikel=text, menge=menge)
            item.save()
        else:
            print("nichts angekommen")

    return redirect('index')        

