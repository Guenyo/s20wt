from django.db import models

# Create your models here.

class Item(models.Model):
    artikel = models.TextField()
    menge = models.TextField()
    objects = models.Manager()

    def __str__(self):
        return "{}, ({})".format(self.artikel, self.menge)
        