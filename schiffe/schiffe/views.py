from django.shortcuts import render, redirect

from . import models

# Create your views here.

def index(request):
    return render(request, "schiffe/index.html")

def benutzer(request):
    benutzers = models.Spieler.objects.all()
    return render(request, "schiffe/benutzer.html", {'benuters':benutzers})


def spieler_eingabe(request):
    if "Weiter" in request.POST:
        #print(request.POST)
        if 'auswahl' in request.POST:
            pass

        name = request.POST.get("spieler_eingabe", "")
        print(request.POST)
        if name:
            spieler = models.Spieler()
            spieler.name = name
            spieler.save()
        else:
            print("keine eingabe")
    if "delete" in request.POST:
        if "auswahl" in request.POST:    
            for item_id in request.POST.getlist('auswahl'):
                    item= models.Spieler.objects.get(name=item_id)
                    item.delete()       
        
            
    return redirect("benutzer")
