from django.shortcuts import render, HttpResponse

from datetime import datetime
from . import models
from random import randint
# Create your views here.


def index1(request):
    return render(request, "coin/1.html")

def index2(request):
   
    wurf = models.Wurf.objects.all().order_by("zeit")
    return HttpResponse(wurf)


def index3(request):
    nummer = randint(0,1)
    context = {}
    if nummer == 0:
        wurf = models.Wurf()
        wurf.ergebnis = "Zahl"
        #wurf.zeit = datetime.now()
        wurf.save()
        context["wurf"] = "Zahl"

    else:
        wurf = models.Wurf()
        wurf.ergebnis = "Kopf"
        #wurf.zeit = datetime.now()
        wurf.save()
        context["wurf"] = "Kopf"
    
    context["kopf"] = models.Wurf.objects.filter(ergebnis="Kopf").count  
    context["zahl"] = models.Wurf.objects.filter(ergebnis__contains="Zahl").count  

    return render(request, "coin/3.html", {"context":context})
      

def index4(request):
    nummer = randint(0,1)
    context = {}
    if nummer == 0:
        wurf = models.Wurf()
        wurf.ergebnis = "Zahl"
        #wurf.zeit = datetime.now()
        wurf.save()
        context["wurf"] = "Zahl"

    else:
        wurf = models.Wurf()
        wurf.ergebnis = "Kopf"
        #wurf.zeit = datetime.now()
        wurf.save()
        context["wurf"] = "Kopf"
    
    context["kopf"] = models.Wurf.objects.filter(ergebnis="Kopf").count  
    context["zahl"] = models.Wurf.objects.filter(ergebnis__contains="Zahl").count  

    return render(request, "coin/4.html", {"context":context})


