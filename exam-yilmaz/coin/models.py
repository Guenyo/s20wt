from django.db import models
import datetime

# Create your models here.
class Wurf(models.Model):
    ergebnis = models.TextField()
    zeit = models.DateTimeField(default=datetime.datetime.now, blank=True)


    def __str__(self):
        return "{} ({})".format(self.ergebnis, self.zeit)
